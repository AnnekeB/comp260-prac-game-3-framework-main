﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour {

	public float startTime = 0.0f;
	private Animator animator;



	void Start () {
		animator = GetComponent<Animator>();
	}
	

	void Update () {
		if (Time.time >= startTime) {
			animator.SetTrigger ("Start");
		}
	}

	void Destroy(){
		Destroy (gameObject);
	}

	void OnCollisionEnter(Collision collision){
		animator.SetTrigger ("Hit");
	}
}
