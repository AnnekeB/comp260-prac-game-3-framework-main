﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

	public float speed = 0.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;


	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
	}
	

	void Update () {
		rigidbody.velocity = speed * direction;
	}

	void OnCollisionEnter (Collision collision) {
		Destroy (gameObject);
	}
}
